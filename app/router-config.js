"use strict";
const movie_list_component_1 = require('./movies/movie-list/movie-list.component');
const movie_editor_component_1 = require('./movies/movie-editor/movie-editor.component');
const indexRoute = {
    path: "",
    component: movie_list_component_1.MovieListComponent
};
const fallbackRoute = {
    path: "**",
    component: movie_list_component_1.MovieListComponent
};
exports.routeConfig = [
    indexRoute,
    {
        path: 'add',
        component: movie_editor_component_1.MovieEditorComponent
    },
    {
        path: 'edit/:id',
        component: movie_editor_component_1.MovieEditorComponent
    },
    fallbackRoute
];
//# sourceMappingURL=router-config.js.map