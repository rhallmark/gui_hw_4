import { Component, Input } from '@angular/core';


@Component({
  selector: 'rating',
  templateUrl: './app/shared/rating/rating.html',
  styleUrls: [ './app/shared/rating/rating.css' ]
})

export class RatingComponent { 

@Input('model') obj: any; //must have parameter rating

}