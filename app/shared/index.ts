// Export shared module itself
export * from './shared.module';

// Export the rating component
export * from './rating/rating.component';

// Export the Validation-message component
export * from './validation-message/validation-message.component';