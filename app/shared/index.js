"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
// Export shared module itself
__export(require('./shared.module'));
// Export the rating component
__export(require('./rating/rating.component'));
// Export the Validation-message component
__export(require('./validation-message/validation-message.component'));
//# sourceMappingURL=index.js.map