import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';

// Custom Shared Componenets
import { RatingComponent } from './rating/rating.component';
import { ValidationMessageComponent } from './validation-message/validation-message.component';

@NgModule({
	id: 'shared',
	imports: [
		BrowserModule,
		RouterModule
	],
	declarations: [
		RatingComponent,
		ValidationMessageComponent
	],
	exports: [
		RatingComponent,
		ValidationMessageComponent,
		RouterModule
	]
})

export class SharedModule { }