import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


// Imports for Domain Module
import { DomainModule } from './domain/index';

// Imports for Movies Module
import { MoviesModule } from './movies/index';

// Imports for shared Module
import { SharedModule } from './shared/index';

// Imports for loading & configuring the in-memory web api
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { MockApiService } from './mock-api.service';


import { AppComponent }   from './app.component';
//import { MovieRepositoryService } from './repositories/movie-repository.service'; **R/F 78**
//import { MovieListComponent }   from './movie-list/movie-list.component'; **R/F 78**
//import { RatingComponent } from './rating/rating.component';
//import { MovieEditorComponent } from './movie-editor/movie-editor.component'; **R/F 78**

@NgModule({
  imports:      [ 
  	BrowserModule,
    CommonModule,
    FormsModule,
    HttpModule,
    InMemoryWebApiModule.forRoot(MockApiService),
    DomainModule.forRoot(),
    MoviesModule,
    SharedModule
  ],
  declarations: [
  	AppComponent

  ],
  //providers: [ MovieRepositoryService ],  **R/F 78**
  bootstrap:    [ AppComponent ]
})


export class AppModule { 

}