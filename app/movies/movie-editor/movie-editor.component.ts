import { Component } from '@angular/core';
import { ActivatedRoute, Router, Params } from '@angular/router';
//import { MovieRepositoryService } from '../repositories/movie-repository.service';
import { FormsModule } from '@angular/forms';
//import { Movie } from '../repositories/movie';
import { Movie, MovieRepositoryService } from '../../domain/index';

/*
* Component for adding a new movie or
* editing a pre-existing movie
*/

@Component({
  selector: 'movie-editor',
  templateUrl: './app/movies/movie-editor/movie-editor.html',
  styleUrls: [ './app/movies/movie-editor/movie-editor.css' ]
})

export class MovieEditorComponent { 

  pageTitle: string;
  movie: Movie;
  old_movie: any;
  years = Array(50).fill(0).map((x,i)=>(new Date().getFullYear()-i));
  l_title: any;
  l_year: any;
  l_imagePath: any;
  l_id: number;

  constructor(private route: ActivatedRoute, //what does activatedRoute do?
              private router: Router,
              private movieRepositoryService : MovieRepositoryService){}

	ngOnInit() {
		this.route.params.forEach( (params) => {
      this.load(+params['id']);
      });
    }

  private load(id){
 		if(!id) {
				this.pageTitle = 'New Movie';
        // Create the movie such that blank properties are defined
        // title, year, imagepath
        this.l_title = "";
        this.l_year;
        this.l_imagePath = "";
  		  this.movie = new Movie(this.l_title,this.l_year,this.l_imagePath);
        this.l_id = this.movie.id; //no id being given here
			  return;
		}
    else if(id) {
        // this.movie = this.movieRepository.getMovie(+params['id']);
        // Need to grab a movie by id. Inside promise set all variables
        this.movieRepositoryService.getMovie(id).then( (response) => {
          this.movie = response;
          this.pageTitle = "Edit Movie:" + this.movie.title;
          this.l_title = this.movie.title;
          this.l_year = this.movie.year;
          this.l_imagePath = this.movie.imagePath;
          this.l_id = this.movie.id;
          });
    }
  }


  save() {

    this.movie = new Movie(this.l_title,this.l_year,this.l_imagePath,this.l_id)

    // If the movie has an ID, update the movie
    if(this.movie.id){
      this.movieRepositoryService.update(this.movie)
        .then(() => this.returnToList(`${this.movie.title} has been updated!`));
    }
    // Otherwise, add the movie as it is new
    // Add the movie to get an ID auto-assigned
    else{
			this.movieRepositoryService.add(this.movie)
				.then(() => this.returnToList(`${this.movie.title} has been added!`));
    }
  }

	private returnToList(message){
		this.router.navigateByUrl("")
			.then(() => alert(message));
	}

}