"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const router_1 = require('@angular/router');
//import { Movie } from '../repositories/movie';
const index_1 = require('../../domain/index');
/*
* Component for adding a new movie or
* editing a pre-existing movie
*/
let MovieEditorComponent = class MovieEditorComponent {
    constructor(route, //what does activatedRoute do?
        router, movieRepositoryService) {
        this.route = route;
        this.router = router;
        this.movieRepositoryService = movieRepositoryService;
        this.years = Array(50).fill(0).map((x, i) => (new Date().getFullYear() - i));
    }
    ngOnInit() {
        this.route.params.forEach((params) => {
            this.load(+params['id']);
        });
    }
    load(id) {
        if (!id) {
            this.pageTitle = 'New Movie';
            // Create the movie such that blank properties are defined
            // title, year, imagepath
            this.l_title = "";
            this.l_year;
            this.l_imagePath = "";
            this.movie = new index_1.Movie(this.l_title, this.l_year, this.l_imagePath);
            this.l_id = this.movie.id; //no id being given here
            return;
        }
        else if (id) {
            // this.movie = this.movieRepository.getMovie(+params['id']);
            // Need to grab a movie by id. Inside promise set all variables
            this.movieRepositoryService.getMovie(id).then((response) => {
                this.movie = response;
                this.pageTitle = "Edit Movie:" + this.movie.title;
                this.l_title = this.movie.title;
                this.l_year = this.movie.year;
                this.l_imagePath = this.movie.imagePath;
                this.l_id = this.movie.id;
            });
        }
    }
    save() {
        this.movie = new index_1.Movie(this.l_title, this.l_year, this.l_imagePath, this.l_id);
        // If the movie has an ID, update the movie
        if (this.movie.id) {
            this.movieRepositoryService.update(this.movie)
                .then(() => this.returnToList(`${this.movie.title} has been updated!`));
        }
        else {
            this.movieRepositoryService.add(this.movie)
                .then(() => this.returnToList(`${this.movie.title} has been added!`));
        }
    }
    returnToList(message) {
        this.router.navigateByUrl("")
            .then(() => alert(message));
    }
};
MovieEditorComponent = __decorate([
    core_1.Component({
        selector: 'movie-editor',
        templateUrl: './app/movies/movie-editor/movie-editor.html',
        styleUrls: ['./app/movies/movie-editor/movie-editor.css']
    }), 
    __metadata('design:paramtypes', [router_1.ActivatedRoute, router_1.Router, index_1.MovieRepositoryService])
], MovieEditorComponent);
exports.MovieEditorComponent = MovieEditorComponent;
//# sourceMappingURL=movie-editor.component.js.map