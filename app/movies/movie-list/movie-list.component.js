"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
//import { Movie } from '../repositories/movie';
const index_1 = require('../../domain/index');
let MovieListComponent = class MovieListComponent {
    constructor(movieRepositoryService) {
        // What I want to do:
        // get an array of movies
        // determine the size of the array of movies
        // 
        // Questions
        // Why is the returned type undefined? Why not object or array of objects? Can't perform lenght on type undef.
        // Is it good practice to pull all the movies at once? Should we just poll through movies once by one eacah giving a response?
        // Is it good practice to have a Movie calss? or should I just leave it as json data? 
        // This follows closely with the Events.. in our project would it be good practice to have an event class?
        this.movieRepositoryService = movieRepositoryService;
        movieRepositoryService.list().then(response => {
            this.movies = response;
            this.movieNumber = this.movies.length;
        });
    }
    updateRating(movie) {
        this.movieRepositoryService.update(movie);
    }
    delete(movie, index) {
        this.movieRepositoryService.delete(movie).then(() => {
            this.movies.splice(index, 1);
            this.movieNumber = this.movieNumber - 1;
            alert(`${movie.title} was deleted!!`);
        });
    }
};
MovieListComponent = __decorate([
    core_1.Component({
        selector: 'movie-list',
        templateUrl: './app/movies/movie-list/movie-list.html',
        styleUrls: ['./app/movies/movie-list/movie-list.css']
    }), 
    __metadata('design:paramtypes', [index_1.MovieRepositoryService])
], MovieListComponent);
exports.MovieListComponent = MovieListComponent;
//# sourceMappingURL=movie-list.component.js.map