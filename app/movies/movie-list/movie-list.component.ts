import { Component } from '@angular/core';
//import { MovieRepositoryService } from '../repositories/movie-repository.service';
import { CommonModule } from '@angular/common';
//import { Movie } from '../repositories/movie';
import { Movie, MovieRepositoryService } from '../../domain/index';



@Component({
  selector: 'movie-list',
  templateUrl: './app/movies/movie-list/movie-list.html',
  styleUrls: [ './app/movies/movie-list/movie-list.css' ]
})

export class MovieListComponent { 
	movies: Movie[];
	movieNumber: number;


	constructor(private movieRepositoryService : MovieRepositoryService){
		// What I want to do:
		// get an array of movies
		// determine the size of the array of movies
		// 
		// Questions
		// Why is the returned type undefined? Why not object or array of objects? Can't perform lenght on type undef.
		// Is it good practice to pull all the movies at once? Should we just poll through movies once by one eacah giving a response?
		// Is it good practice to have a Movie calss? or should I just leave it as json data? 
		// This follows closely with the Events.. in our project would it be good practice to have an event class?

		movieRepositoryService.list().then(response => {
			this.movies = response;
			this.movieNumber = this.movies.length;
		});
	}

	updateRating(movie){
		this.movieRepositoryService.update(movie);
	}

	delete(movie,index){

		this.movieRepositoryService.delete(movie).then(() => {
			this.movies.splice(index,1);
			this.movieNumber = this.movieNumber - 1;
			alert(`${movie.title} was deleted!!`)
		});

	}

}