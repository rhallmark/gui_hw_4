// This is the movies module itself
export * from './movies.module';

export * from './movie-editor/movie-editor.component';
export * from './movie-list/movie-list.component';