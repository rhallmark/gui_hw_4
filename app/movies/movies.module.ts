import { NgModule, ModuleWithProviders } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieEditorComponent } from './movie-editor/movie-editor.component';

import {routeConfig} from '../router-config';

import { SharedModule } from '../shared/index';

@NgModule({
	id: 'movies',
	imports: [
        BrowserModule,
        CommonModule,
        FormsModule,
        HttpModule,
        RouterModule.forRoot(routeConfig),
        SharedModule
	],
    declarations:[
		MovieListComponent,
        MovieEditorComponent
    ],
	exports: [
        RouterModule
	]
})

export class MoviesModule {

}