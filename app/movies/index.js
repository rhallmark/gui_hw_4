"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
// This is the movies module itself
__export(require('./movies.module'));
__export(require('./movie-editor/movie-editor.component'));
__export(require('./movie-list/movie-list.component'));
//# sourceMappingURL=index.js.map