"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const platform_browser_1 = require('@angular/platform-browser');
const common_1 = require('@angular/common');
const router_1 = require('@angular/router');
const forms_1 = require('@angular/forms');
const http_1 = require('@angular/http');
const movie_list_component_1 = require('./movie-list/movie-list.component');
const movie_editor_component_1 = require('./movie-editor/movie-editor.component');
const router_config_1 = require('../router-config');
const index_1 = require('../shared/index');
let MoviesModule = class MoviesModule {
};
MoviesModule = __decorate([
    core_1.NgModule({
        id: 'movies',
        imports: [
            platform_browser_1.BrowserModule,
            common_1.CommonModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot(router_config_1.routeConfig),
            index_1.SharedModule
        ],
        declarations: [
            movie_list_component_1.MovieListComponent,
            movie_editor_component_1.MovieEditorComponent
        ],
        exports: [
            router_1.RouterModule
        ]
    }), 
    __metadata('design:paramtypes', [])
], MoviesModule);
exports.MoviesModule = MoviesModule;
//# sourceMappingURL=movies.module.js.map