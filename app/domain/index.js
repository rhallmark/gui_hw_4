"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
// This is the domain module itself
__export(require('./domain.module'));
// This is the only class or 'type'
__export(require('./movies/movie'));
// This is the only repository
__export(require('./movies/movie-repository.service'));
//# sourceMappingURL=index.js.map