import { NgModule, ModuleWithProviders } from '@angular/core';
import { HttpModule } from '@angular/http';

//import { ApiConfig } from '../core/index';

//import { AccountsRepository } from './repositories/accounts-repository.service';
//import { DepartmentsRepository } from './repositories/departments-repository.service';

import { MovieRepositoryService } from './movies/movie-repository.service';

@NgModule({
	id: 'domain',
	imports: [
		HttpModule,
	],
	providers: [
		MovieRepositoryService
	]
})



export class DomainModule {
	static forRoot(): ModuleWithProviders {
		return {
			ngModule: DomainModule,
			providers: [
			]
		};
	}
}