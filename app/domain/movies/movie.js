"use strict";
class Movie {
    constructor(title, year, imagePath, id) {
        this.id = id;
        this.title = title;
        this.year = year;
        this.imagePath = imagePath;
        this.rating = 0;
    }
    //Theoretically these are not needed because vars aren't private?
    getTitle() {
        return this.title;
    }
    getYear() {
        return this.year;
    }
    getImagePath() {
        return this.imagePath;
    }
    getRating() {
        return this.rating;
    }
    setRating(ratingInput) {
        this.rating = ratingInput;
    }
    hasId() {
        if (this.id) {
            return true;
        }
        else {
            return false;
        }
    }
}
exports.Movie = Movie;
//# sourceMappingURL=movie.js.map