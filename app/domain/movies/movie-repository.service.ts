import { Injectable } from '@angular/core';
import { Movie } from './movie';
import { Http, Headers, Response } from '@angular/http';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class MovieRepositoryService {

	private _apiUrl = 'app/movies';

	constructor(private http: Http){

	}

	public list() : Promise<Movie[]> {
		//return this._movies;
		return this.http.get(this._apiUrl)
			.toPromise()
			.then(response => response.json().data as Movie[]);
	}

	//This should get a movie based upon an id
	public getMovie(id : number) : Promise<any> {
		//var index = this.getIndex(id);
		//return this._movies[index];
		var pluck = (response) => (response && response.length) ? response[0] : undefined;
		return this.http.get(`${this._apiUrl}/?id=${id}`)
			.toPromise()
			.then((response) => pluck(response.json().data))
			.catch((response) => alert(response.json().error));
	}

	public add(movie) : Promise<any> {
		//posting json data? how does this work as posting a movie object?
		return this.http.post(this._apiUrl, movie)
			.toPromise()
			.then(() => movie) 
			.catch(response => alert(response.json().error));
	}

	public update(movie) : Promise<any> {

		return this.http.put(`${this._apiUrl}/${movie.id}`, movie)
			.toPromise()
			.then(() => movie)
			.catch(response => alert(response.json().error));
	}

	public delete(movie) : Promise<void>{

		return this.http.delete(`${this._apiUrl}/${movie.id}`,movie)
		.toPromise()
		.catch(response => alert(response.json().error));
	}
}
