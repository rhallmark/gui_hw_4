
export class Movie {
    id: number;
    title: string;
    year: number;
    imagePath: string;
    rating: number;

	constructor(title:string, year:number, imagePath:string, id?: number){
		this.id = id;
        this.title = title;
        this.year = year;
        this.imagePath = imagePath;
        this.rating = 0;
	}    
    

    //Theoretically these are not needed because vars aren't private?
    getTitle(): string {
        return this.title;
    }

    getYear(): number {
        return this.year;
    }

    getImagePath(): string {
        return this.imagePath;
    }

    getRating(): number{
        return this.rating;
    }

    setRating(ratingInput: number){
        this.rating = ratingInput;
    }

    hasId(): boolean{
        if(this.id){
            return true;
        }
        else{
            return false;
        }
    }
}