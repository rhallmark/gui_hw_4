// This is the domain module itself
export * from './domain.module';

// This is the only class or 'type'
export * from './movies/movie';

// This is the only repository
export * from './movies/movie-repository.service';