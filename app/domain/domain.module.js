"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const http_1 = require('@angular/http');
//import { ApiConfig } from '../core/index';
//import { AccountsRepository } from './repositories/accounts-repository.service';
//import { DepartmentsRepository } from './repositories/departments-repository.service';
const movie_repository_service_1 = require('./movies/movie-repository.service');
let DomainModule_1 = class DomainModule {
    static forRoot() {
        return {
            ngModule: DomainModule_1,
            providers: []
        };
    }
};
let DomainModule = DomainModule_1;
DomainModule = DomainModule_1 = __decorate([
    core_1.NgModule({
        id: 'domain',
        imports: [
            http_1.HttpModule,
        ],
        providers: [
            movie_repository_service_1.MovieRepositoryService
        ]
    }), 
    __metadata('design:paramtypes', [])
], DomainModule);
exports.DomainModule = DomainModule;
//# sourceMappingURL=domain.module.js.map