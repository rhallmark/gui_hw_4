"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require('@angular/core');
const platform_browser_1 = require('@angular/platform-browser');
const common_1 = require('@angular/common');
const forms_1 = require('@angular/forms');
const http_1 = require('@angular/http');
// Imports for Domain Module
const index_1 = require('./domain/index');
// Imports for Movies Module
const index_2 = require('./movies/index');
// Imports for shared Module
const index_3 = require('./shared/index');
// Imports for loading & configuring the in-memory web api
const angular_in_memory_web_api_1 = require('angular-in-memory-web-api');
const mock_api_service_1 = require('./mock-api.service');
const app_component_1 = require('./app.component');
//import { MovieRepositoryService } from './repositories/movie-repository.service'; **R/F 78**
//import { MovieListComponent }   from './movie-list/movie-list.component'; **R/F 78**
//import { RatingComponent } from './rating/rating.component';
//import { MovieEditorComponent } from './movie-editor/movie-editor.component'; **R/F 78**
let AppModule = class AppModule {
};
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            common_1.CommonModule,
            forms_1.FormsModule,
            http_1.HttpModule,
            angular_in_memory_web_api_1.InMemoryWebApiModule.forRoot(mock_api_service_1.MockApiService),
            index_1.DomainModule.forRoot(),
            index_2.MoviesModule,
            index_3.SharedModule
        ],
        declarations: [
            app_component_1.AppComponent
        ],
        //providers: [ MovieRepositoryService ],  **R/F 78**
        bootstrap: [app_component_1.AppComponent]
    }), 
    __metadata('design:paramtypes', [])
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map