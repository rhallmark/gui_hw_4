import {Route} from "@angular/router";

// Components
import { AppComponent }   from './app.component';
import { MovieListComponent }   from './movies/movie-list/movie-list.component';
import { RatingComponent } from './shared/rating/rating.component';
import { MovieEditorComponent } from './movies/movie-editor/movie-editor.component';


const indexRoute: Route = {
    path: "",
    component: MovieListComponent
}

const fallbackRoute: Route = {
    path: "**",
    component: MovieListComponent
}

export const routeConfig: Route[] = [
    indexRoute,
    {
        path: 'add',
        component: MovieEditorComponent
    },
    {
        path: 'edit/:id',
        component: MovieEditorComponent
    },
    fallbackRoute
];